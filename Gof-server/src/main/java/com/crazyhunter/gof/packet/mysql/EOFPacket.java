package com.crazyhunter.gof.packet.mysql;

import java.nio.ByteBuffer;

import com.crazyhunter.gof.connection.ServerConnection;
import com.crazyhunter.gof.util.ByteBufferUtil;
 
public class EOFPacket extends MySQLPacket {
    public static final byte FIELD_COUNT = (byte) 0xfe;

    public byte fieldCount = FIELD_COUNT;
    public int warningCount;
    public int status = 2;

    @Override
    public void write(ByteBuffer buffer) {

        int size = calcPacketSize();
        //buffer = c.checkWriteBuffer(buffer, c.getPacketHeaderSize() + size);
        ByteBufferUtil.writeUB3(buffer, size);
        buffer.put(packetNum);
        buffer.put(fieldCount);
        ByteBufferUtil.writeUB2(buffer, warningCount);
        ByteBufferUtil.writeUB2(buffer, status);
//字节流,1byte=8bit 一个字characet=2byte 一个int占32位,就是4个byte
    }

    @Override
    public int getLength() {
        return ByteBufferUtil.getLength(fieldCount);
    }

    public void read(byte[] data) {
        MySQLMessage mm = new MySQLMessage(data);
        packetLen = mm.readUB3();
        packetNum = mm.read();
        fieldCount = mm.read();
        warningCount = mm.readUB2();
        status = mm.readUB2();
    }



//    @Override
    public int calcPacketSize() {
        return 5;// 1+2+2;  包的序号不算入包长度, 每个MySQL数据包固定前四位：前三位是包长度，第四位是包的序号。  所以fieldCount 占一个字节, warningCount占两个字节,
        // status占两个字节.一共5个字节
    }
//
//    @Override
    protected String getPacketInfo() {
        return "MySQL EOF Packet";
    }

}