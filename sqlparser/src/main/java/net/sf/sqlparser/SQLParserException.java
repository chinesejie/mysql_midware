package net.sf.sqlparser;

/**
 * SQL解析异常类.
 * @author lixiangyang.
 *
 */
public class SQLParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SQLParserException() {
		super();
	}
	
	public SQLParserException(String message) {
		super(message);
	}
	
	public SQLParserException(Throwable cause) {
		super(cause);
	}
	
	public SQLParserException(String message, Throwable cause) {
		super(message, cause);
	}
}
