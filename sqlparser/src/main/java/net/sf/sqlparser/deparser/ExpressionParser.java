package net.sf.sqlparser.deparser;

import net.sf.jsqlparser.util.deparser.ExpressionDeParser;

/**
 * 表达式解析类，继承自JSqlParser的ExpressionDeParser
 * @author lixiangyang
 *
 */
public class ExpressionParser extends ExpressionDeParser {

    public ExpressionParser() {
    }
    
    public ExpressionParser(SelectParser selectVisitor, StringBuffer buffer) {
        super(selectVisitor, buffer);
    }
    
    public void setSelectVisitor(SelectParser visitor) {
        super.setSelectVisitor(visitor);
    }
    
    // TODO
    
}
