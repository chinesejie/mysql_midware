package net.sf.sqlparser.parser;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.sqlparser.util.SQLParserUtils;

import org.apache.log4j.Logger;

public class TestWhereParser {
	
	/**
	 * 日志.
	 */
	private static Logger log = Logger.getLogger(TestWhereParser.class);

	/**
	 * @param args
	 * @throws JSQLParserException 
	 */
	public static void main(String[] args) throws JSQLParserException {
		final String whereString = "WHERE E.ENTITY_ID = P.ENTITY_ID AND E.ENTITY_ID = '123'";
		final Expression whrExp = SQLParserUtils.parserWhere(whereString);
		log.debug("解析后的条件语句 = " + whrExp);
	}

}
