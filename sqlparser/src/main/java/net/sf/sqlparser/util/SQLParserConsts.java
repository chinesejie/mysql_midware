package net.sf.sqlparser.util;

import net.sf.sqlparser.parser.SQLParserTreeConstants;

/**
 * SQL解析常量类.
 * @author lixiangyang
 *
 */
public interface SQLParserConsts {

	/**
	 * 表名节点 - Table
	 */
	public static final String NODE_TABLE = 
			SQLParserTreeConstants.jjtNodeName[SQLParserTreeConstants.JJTTABLE];
	
	/**
	 * 列名节点 - Column
	 */
	public static final String NODE_COLUMN = 
			SQLParserTreeConstants.jjtNodeName[SQLParserTreeConstants.JJTCOLUMN];
}
