package net.sf.sqlparser.visitor;

/**
 * Visitor抽象类，所有Visitor需要继承此类
 * @author lixiangyang
 *
 */
public abstract class AbstractVisitor {

	protected VisitorContext context;
	
	public AbstractVisitor(final VisitorContext ctx) {
		this.context = ctx;
	}
}
