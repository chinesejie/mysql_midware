package net.sf.sqlparser.visitor;

import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.InverseExpression;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.SubSelect;

public class ExpressionVisitorImpl extends AbstractVisitor implements ExpressionVisitor {

    public ExpressionVisitorImpl(VisitorContext ctx) {
        super(ctx);
    }

    public void visit(NullValue nullValue) {
    }

    public void visit(Function function) {
    }

    public void visit(InverseExpression inverseExprression) {
    }

    public void visit(JdbcParameter jdbcParameter) {
    }

    public void visit(DoubleValue doubleValue) {
    }

    public void visit(LongValue longValue) {
    }

    public void visit(DateValue dateValue) {
    }

    public void visit(TimeValue timeValue) {
    }

    public void visit(TimestampValue timestampValue) {
    }

    public void visit(Parenthesis parenthesis) {
        ExpressionVisitorImpl expressionVisitor = new ExpressionVisitorImpl(context);
        parenthesis.getExpression().accept(expressionVisitor);
    }

    public void visit(StringValue stringValue) {
    }

    public void visit(Addition addition) {
    }

    public void visit(Division division) {
    }

    public void visit(Multiplication multiplication) {
    }

    public void visit(Subtraction subtraction) {
    }

    public void visit(AndExpression andExpression) {
        ExpressionVisitorImpl left = new ExpressionVisitorImpl(context);
        andExpression.getLeftExpression().accept(left);
        
        ExpressionVisitorImpl right = new ExpressionVisitorImpl(context);
        andExpression.getRightExpression().accept(right);
    }

    public void visit(OrExpression orExpression) {
        ExpressionVisitorImpl left = new ExpressionVisitorImpl(context);
        orExpression.getLeftExpression().accept(left);
        
        ExpressionVisitorImpl right = new ExpressionVisitorImpl(context);
        orExpression.getRightExpression().accept(right);
    }

    public void visit(Between between) {
        between.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        between.getBetweenExpressionStart().accept(new ExpressionVisitorImpl(context));
        between.getBetweenExpressionEnd().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(EqualsTo equalsTo) {
        equalsTo.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        equalsTo.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(GreaterThan greaterThan) {
        greaterThan.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        greaterThan.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(GreaterThanEquals greaterThanEquals) {
        greaterThanEquals.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        greaterThanEquals.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(InExpression inExpression) {
        ItemsList list = inExpression.getItemsList();
        list.accept(new ItemsListVisitorImpl(context));
    }

    public void visit(IsNullExpression isNullExpression) {
    }

    public void visit(LikeExpression likeExpression) {
        likeExpression.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        likeExpression.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(MinorThan minorThan) {
        minorThan.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        minorThan.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(MinorThanEquals minorThanEquals) {
        minorThanEquals.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        minorThanEquals.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(NotEqualsTo notEqualsTo) {
        notEqualsTo.getLeftExpression().accept(new ExpressionVisitorImpl(context));
        notEqualsTo.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    /**
     * 处理Column
     */
    public void visit(Column column) {
    	// TODO
    	System.out.println("列名：" + column.getWholeColumnName());
    }

    public void visit(SubSelect subSelect) {
        subSelect.getSelectBody().accept(new SelectVisitorImpl(context));
    }

    public void visit(CaseExpression caseExpression) {
    }

    public void visit(WhenClause whenClause) {
    }

    public void visit(ExistsExpression existsExpression) {
    }

    public void visit(AllComparisonExpression allComparisonExpression) {
    }

    public void visit(AnyComparisonExpression anyComparisonExpression) {
    }

    /**
     * 处理||连字符
     */
    public void visit(Concat concat) {
    	concat.getLeftExpression().accept(new ExpressionVisitorImpl(context));
    	concat.getRightExpression().accept(new ExpressionVisitorImpl(context));
    }

    public void visit(Matches matches) {
    }

    public void visit(BitwiseAnd bitwiseAnd) {
    }

    public void visit(BitwiseOr bitwiseOr) {
    }

    public void visit(BitwiseXor bitwiseXor) {
    }
}

