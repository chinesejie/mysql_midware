package net.sf.sqlparser.visitor;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.sqlparser.util.SQLParserUtils;

public class FromItemVisitorImpl extends AbstractVisitor implements FromItemVisitor {
	
    public FromItemVisitorImpl(VisitorContext ctx) {
        super(ctx);
    }

    /**
     * 处理 Table
     */
    public void visit(Table table) {
    	System.out.println("表名：" + table.getName());
    	final Expression expWhr = SQLParserUtils.findWhereByFromItem(table, context.getStatementNode());
		if (expWhr != null) {
			System.out.println("查找到的Where条件部分 = " + expWhr);
		} else {
			System.out.println("没有找到Where条件部分");
		}
    }

    public void visit(SubSelect subSelect) {
    	subSelect.getSelectBody().accept(new SelectVisitorImpl(context));
    }

    public void visit(SubJoin subJoin) {
        Join join = subJoin.getJoin();
        join.getRightItem().accept(new FromItemVisitorImpl(context));
        join.getOnExpression().accept(new ExpressionVisitorImpl(context));
    }
}

