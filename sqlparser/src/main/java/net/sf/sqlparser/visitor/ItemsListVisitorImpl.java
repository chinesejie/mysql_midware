package net.sf.sqlparser.visitor;

import java.util.Iterator;
import java.util.List;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.statement.select.SubSelect;

public class ItemsListVisitorImpl extends AbstractVisitor implements ItemsListVisitor {

    public ItemsListVisitorImpl(VisitorContext ctx) {
        super(ctx);
    }

    public void visit(SubSelect subSelect) {
        subSelect.getSelectBody().accept(new SelectVisitorImpl(context));
    }

    public void visit(ExpressionList expList) {
        List list = expList.getExpressions();
        if (list != null && list.size() > 0) {
            for (Iterator it = list.iterator(); it.hasNext();) {
            	Expression expr = (Expression) it.next();
                expr.accept(new ExpressionVisitorImpl(context));
            }
        }
    }
}