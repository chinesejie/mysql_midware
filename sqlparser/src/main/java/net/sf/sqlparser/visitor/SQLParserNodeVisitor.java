package net.sf.sqlparser.visitor;

import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.sqlparser.parser.SQLParserVisitor;
import net.sf.sqlparser.parser.SimpleNode;
import net.sf.sqlparser.util.SQLParserConsts;

import org.apache.log4j.Logger;

/**
 * SQL解析节点访问者.
 * @author lixiangyang
 *
 */
public class SQLParserNodeVisitor extends AbstractVisitor implements SQLParserVisitor {
	
	/**
	 * 日志.
	 */
	private static Logger log = Logger.getLogger(SQLParserNodeVisitor.class);
	
	public SQLParserNodeVisitor(final VisitorContext ctx) {
		super(ctx);
	}

	public Object visit(final SimpleNode node, final Object data) {
		final Object value = node.jjtGetValue();
		
		if (value != null) {
			if (SQLParserConsts.NODE_TABLE.equals(node.toString())) {
				process((Table) value);
			} else if (SQLParserConsts.NODE_COLUMN.equals(node.toString())) {
				process((Column) value);
			}
		}
		
		return node.childrenAccept(this, data);
	}
	
	private void process(final Table table) {
		// 打印所有表名
		log.debug("Table = " + table.getWholeTableName());
	}
	
	private void process(final Column column) {
		// 打印所有列名
		log.debug("Column = " + column.getWholeColumnName());
	}

}
