package net.sf.sqlparser.visitor;

import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;

public class SelectItemVisitorImpl extends AbstractVisitor implements
		SelectItemVisitor {

	public SelectItemVisitorImpl(VisitorContext ctx) {
		super(ctx);
	}

	public void visit(AllColumns allColumns) {
	}

	public void visit(AllTableColumns allTableColumns) {
	}

	/**
	 * 处理SelectItem部分表达式
	 */
	public void visit(SelectExpressionItem selectExpressionItem) {
		selectExpressionItem.getExpression().accept(new ExpressionVisitorImpl(context));  
	}

}
