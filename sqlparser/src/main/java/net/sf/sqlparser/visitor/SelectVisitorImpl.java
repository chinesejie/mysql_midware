package net.sf.sqlparser.visitor;

import java.util.Iterator;
import java.util.List;

import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.Union;

public class SelectVisitorImpl extends AbstractVisitor implements SelectVisitor {

    public SelectVisitorImpl(VisitorContext ctx) {
        super(ctx);
    }

    public void visit(PlainSelect plainSelect) {
        // SELECT ITEM访问
        List selectItems = plainSelect.getSelectItems();
        for (Iterator it = selectItems.iterator(); it.hasNext();) {
        	SelectItem item = (SelectItem) it.next();
            item.accept(new SelectItemVisitorImpl(context));
        }

        // FROM访问
        FromItem from = plainSelect.getFromItem();
        FromItemVisitorImpl fromItemVisitor = new FromItemVisitorImpl(context);
        from.accept(fromItemVisitor);

        // 查询条件访问
        if (plainSelect.getWhere() != null) {
            plainSelect.getWhere().accept(new ExpressionVisitorImpl(context));
        }
        
        // JOIN表的访问
        List joins = plainSelect.getJoins();
        if (joins != null && joins.size() > 0) {
            for (Iterator it = joins.iterator(); it.hasNext();) {
            	Join join = (Join) it.next();
                FromItemVisitorImpl tempfv = new FromItemVisitorImpl(context);
                join.getRightItem().accept(tempfv);
                if (join.getOnExpression() != null) {
                    join.getOnExpression().accept(new ExpressionVisitorImpl(context));
                }
            }
        }

        // ORDER BY 访问
        List elements = plainSelect.getOrderByElements();
        if (elements != null && elements.size() > 0) {
            for (Iterator it = elements.iterator(); it.hasNext();) {
            	OrderByElement orderByElement = (OrderByElement) it.next();
            	orderByElement.getExpression().accept(new ExpressionVisitorImpl(context));
            }
        }

        // GROUP BY的HAVING访问
        if (plainSelect.getHaving() != null) {
            plainSelect.getHaving().accept(new ExpressionVisitorImpl(context));
        }
    }

    public void visit(Union union) {
        List selects = union.getPlainSelects();
        for (Iterator it = selects.iterator(); it.hasNext();) {
        	PlainSelect select = (PlainSelect) it.next();
            select.accept(new SelectVisitorImpl(context));
        }
        
        List elements = union.getOrderByElements();
        if (elements != null && elements.size() > 0) {
            for (Iterator it = elements.iterator(); it.hasNext();) {
            	OrderByElement orderByElement = (OrderByElement) it.next();
            	orderByElement.getExpression().accept(new ExpressionVisitorImpl(context));
            }
        }
    }
}
