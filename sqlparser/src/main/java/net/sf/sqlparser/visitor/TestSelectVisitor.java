package net.sf.sqlparser.visitor;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.sqlparser.parser.SQLParserResult;
import net.sf.sqlparser.util.SQLParserUtils;

public class TestSelectVisitor {
	
	/**
	 * @param args
	 * @throws JSQLParserException 
	 */
	public static void main(String[] args) throws JSQLParserException {
		final String selectString = "SELECT P.NAME AS A, P.CHN_NAME FROM RC_ENTITY E, RC_ENTITY_PROP P"
								+ " WHERE E.ENTITY_ID = P.ENTITY_ID AND E.ENTITY_ID = '123'";
		final SQLParserResult result = SQLParserUtils.parseWithTree(selectString);
		final Select select = (Select) result.getStmt();
		
		final VisitorContext ctx = new VisitorContext();
		ctx.setStatementNode(result.getStatementNode());
		
		final SelectVisitor selectVisitor = new SelectVisitorImpl(ctx);
		select.getSelectBody().accept(selectVisitor);
	}
}
