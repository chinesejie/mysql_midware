package net.sf.sqlparser.visitor;

import net.sf.sqlparser.parser.SimpleNode;

/**
 * Visit上下文
 * @author lixiangyang
 *
 */
public class VisitorContext {

	/**
	 * SQL解析后生成的树节点 .
	 */
	private SimpleNode statementNode;

	public SimpleNode getStatementNode() {
		return statementNode;
	}

	public void setStatementNode(SimpleNode statementNode) {
		this.statementNode = statementNode;
	}
}
